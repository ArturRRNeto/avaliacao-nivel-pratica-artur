# file:        cli.py
# author:      Artur Rodrigues Rocha Neto
# email:       artur.rodrigues26@gmail.com
# created:     23/01/2021
# description: Interface de linha de comando para o sistema de NR-IQA proposto.

from nriqa import *
from model import *

import sys

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("ERRO: número incorreto de parâmetros!")
        print("USO:  python3 cli.py <arquivo_imagem>")
    
    label, class_name, score = estimator(sys.argv[1])
    
    print("Distorção: {} ({})".format(label, class_name))
    print("Qualidade: {}".format(score))
    
