# file:        features.py
# author:      Artur Rodrigues Rocha Neto
# email:       artur.rodrigues26@gmail.com
# created:     21/01/2021
# description: Funções de extração de atributo.

from nriqa import *

import os
import cv2
import parse
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from skimage import io
from skimage import color
from itertools import chain
from scipy.special import gamma

def mos_names_csv():
    """
    mos_names_csv()
    Gera dataframe com metadados nome de arquivo e MOS.
    
        Parâmetros
        ----------
            Nenhum.
        Retorno
        -------
            metadata: dataframe.
    """
    
    metadata = {}
    comp = parse.compile("i{}_{}_{}.bmp")
    with open(MOS_NAMES_FILE, "r") as f:
        lines = f.readlines()
        metadata["imgfile"] = [x.strip().split()[1].lower() for x in lines]
        metadata["mos"] = [float(x.strip().split()[0]) for x in lines]
    
    metadata = pd.DataFrame(metadata)
    return metadata

def load_image_TID2008(filename, as_gray=False):
    """
    load_image_TID2008(filename, as_gray=False)
    Carrega uma imagem no formato da base TID2008.
    
        Parâmetros
        ----------
            filename: caminho completo para a image.
            as_gray: True se as imagens devem ser automaticamente convertidas a escala de cinza no carregamento.
        Retorno
        -------
            img: a matriz da imagem.
            imgfile: o nome base do arquivo da imagem.
            distortion: a classe de distorção a qual a imagem pertence.
    """
    
    imgfile = os.path.basename(filename)
    img = io.imread(filename, as_gray=as_gray)
    return img, imgfile, parse.compile("i{}_{}_{}.bmp").parse(imgfile.lower())[1]

def image_preproc_mscn(img):
    """
    image_preproc_mscn(img)
    Normalização de constraste por diferença de média e variância.
    
        Parâmetros
        ----------
            img: a imagem a ser normalizada.
        Retorno
        -------
            structdis: imagem normalizada.
    """
    
    kernel = (7, 7)
    sval = 7.0 / 6.0
    
    blurred = cv2.GaussianBlur(img, kernel, sval, borderType=cv2.BORDER_CONSTANT)
    blurred_sq = blurred * blurred
    
    sigma = cv2.GaussianBlur(img * img, kernel, sval, borderType=cv2.BORDER_CONSTANT)
    sigma = np.sqrt(np.abs(sigma - blurred_sq)) + (1.0 / 255.0)
    
    structdis = (img - blurred) / sigma
    
    return structdis

def plot_mscn(filename):
    """
    plot_mscn(filename)
    Mostra normalização MSCN para do mapa de intensidade (tom de cinza) de uma imagem.
    
        Parâmetros
        ----------
            filename: caminho para imagem.
        Retorno
        -------
            Nada.
    """
    
    img = io.imread(filename, as_gray=True)
    mscn = image_preproc_mscn(img)
    
    plt.figure(figsize=(8, 6))
    plt.axis("off")
    plt.imshow(mscn, cmap="gray")
    plt.tight_layout()
    plt.savefig(os.path.join(FIGURES_FOLDER, "MSCN_single.png"))
    plt.close()

def plot_mscn_rgb(filename):
    """
    plot_mscn_rgb(filename)
    Mostra normalização MSCN para cada canal de uma imagem.
    
        Parâmetros
        ----------
            filename: caminho para imagem.
        Retorno
        -------
            Nada.
    """
    
    img = io.imread(filename)
    
    sns.set()
    fig, axes = plt.subplots(3, 3, sharex=True, figsize=(10, 8))
    fig.suptitle("Geração de imagem normalizada por canal")
    
    # colorida
    axes[0, 1].set_title("Entrada")
    axes[0, 1].axis("off")
    axes[0, 1].imshow(img, cmap="gray")
    
    # canais
    axes[1, 0].set_title("R")
    axes[1, 0].axis("off")
    axes[1, 0].imshow(img[:,:,0], cmap="gray")
    
    axes[1, 1].set_title("G")
    axes[1, 1].axis("off")
    axes[1, 1].imshow(img[:,:,1], cmap="gray")
    
    axes[1, 2].set_title("B")
    axes[1, 2].axis("off")
    axes[1, 2].imshow(img[:,:,2], cmap="gray")
    
    # MSCNs
    axes[2, 0].set_title("R_mscn")
    axes[2, 0].axis("off")
    axes[2, 0].imshow(image_preproc_mscn(img[:,:,0] / 255.0), cmap="gray")
    
    axes[2, 1].set_title("G_mscn")
    axes[2, 1].axis("off")
    axes[2, 1].imshow(image_preproc_mscn(img[:,:,1] / 255.0), cmap="gray")
    
    axes[2, 2].set_title("B_mscn")
    axes[2, 2].axis("off")
    axes[2, 2].imshow(image_preproc_mscn(img[:,:,2] / 255.0), cmap="gray")
    
    # OFF
    axes[0, 0].axis("off")
    axes[0, 2].axis("off")
    
    plt.tight_layout()
    plt.savefig(os.path.join(FIGURES_FOLDER, "MSCN_rgb.png"))
    plt.close()

def gdd_fit(img):
    """
    gdd_fit(img)
    Ajusta as intensidades de uma imagem a uma distribuição gaussiana geral simétrica.
    
    Parâmetros
        ----------
            img: a imagem a ser ajustada.
        Retorno
        -------
            alpha: atributo de forma da distribuição.
            std: variância.
    """
    
    gam = np.arange(0.2, 10 + 0.001, 0.001)
    r_gam = (gamma(1.0 / gam) * gamma(3.0 / gam) / (gamma(2.0 / gam)**2))
    
    std_sq = np.mean(img**2)
    std = np.sqrt(std_sq)
    mu = np.mean(np.abs(img))
    rho = std_sq / (mu**2)
    
    diff = np.abs(rho - r_gam)
    index = np.argmin(diff)
    alpha = gam[index]
    
    return alpha, std

def agdd_fit(img):
    """
    agdd_fit(img)
    Ajusta as intensidades de uma imagem a uma distribuição gaussiana geral assimétrica.
    
        Parâmetros
        ----------
            img: a imagem a ser ajustada.
        Retorno
        -------
            upsilon: atributo de forma da distribuição.
            left_std: variância à esquerda.
            right_std: variância à direita.
    """
    
    gam = np.arange(0.2, 10 + 0.001, 0.001)
    r_gam = ((gamma(2.0 / gam)) ** 2) / (gamma(1.0 / gam) * gamma(3.0 / gam))
    
    left_std = np.sqrt(np.mean((img[img < 0])**2))
    right_std = np.sqrt(np.mean((img[img > 0])**2))
    gamma_hat = left_std / right_std
    rhat = (np.mean(np.abs(img)))**2 / np.mean(img**2)
    rhat_norm = (rhat * ((gamma_hat**3) + 1) * (gamma_hat + 1)) / (((gamma_hat**2) + 1)**2)
    
    diff = (r_gam - rhat_norm)**2
    index = np.argmin(diff)
    upsilon = gam[index]
    
    return upsilon, left_std, right_std

def brisque_features(img):
    """
    brisque_features(img)
    Calcula os atributos BRISQUE de uma imagem (inspiração: https://github.com/bukalapak/pybrisque)
    
        Parâmetros
        ----------
            img: imagem de onde extrair os atributos.
        Retorno
        -------
            features: atributos baseados em ajuste de gaussianas.
    """
    
    scales = 2
    features = []
    img_src = np.copy(img)
    
    for scale in range(scales):
        structdis = image_preproc_mscn(img_src)
        
        alpha, std = gdd_fit(structdis)
        features.extend([alpha, std**2])
        
        shifts = [[0, 1], [1, 0], [1, 1], [-1, 1]]
        for shift in shifts:
            shifted_structdis = np.roll(np.roll(structdis, shift[0], axis=0), shift[1], axis=1)
            pair = np.ravel(structdis, order="F") * np.ravel(shifted_structdis, order="F")
            upsilon, left_var, right_var = agdd_fit(pair)
            
            const = np.sqrt(gamma(1.0 / upsilon)) / np.sqrt(gamma(3.0 / upsilon))
            eta = (right_var - left_var) * (gamma(2.0 / upsilon) / gamma(1.0 / upsilon)) * const
            
            features.extend([upsilon, eta, left_var**2, right_var**2])
        
        img_src = cv2.resize(img_src, (0, 0), fx=0.5, fy=0.5, interpolation=cv2.INTER_CUBIC)
    
    return features

def brisque_rgb_features(img):
    """
    brisque_rgb_features(img)
    Calcula os atributos BRISQUE de cada canal RGB de uma imagem.
    
        Parâmetros
        ----------
            img: imagem RBG completa de onde extrair os atributos.
        Retorno
        -------
            features: concatenação de atributos baseados em ajuste de gaussianas de cada canal da imagem.
    """
    
    features = [brisque_features(img[:,:,i] / 255.0) for i in range(3)]
    features = list(chain.from_iterable(features))
    
    return features

def generate_features(feat_func, feat_file, as_gray=False):
    """
    generate_features(feat_func, feat_file, as_gray=False)
    Função genérica que toma um extrator de atributos e o executa ao longo de toda a base TID2008.
    
        Parâmetros
        ----------
            feat_func: uma função extratora que toma como entrada única uma matriz de imagem.
            feat_file: caminho para arquivo onde os atributos ficarão armazenados.
            as_gray: True se as imagens devem ser automaticamente convertidas a escala de cinza no carregamento.
        Retorno
        -------
            Nada.
    """
    
    dump = []
    num_images = len(os.listdir(DISTORTED_FOLDER))
    for index, filename in enumerate(os.listdir(DISTORTED_FOLDER)):
        filename = os.path.join(DISTORTED_FOLDER, filename)
        img, imgfile, distortion = load_image_TID2008(filename, as_gray)
        features = feat_func(img)
        dump.append([imgfile.lower(), distortion] + features)
        
        print(f"{index+1}/{num_images}", end="\r")
    
    columns = ["imgfile", "distortion"] + [f"f{i}" for i in range(len(dump[0]) - 2)]
    df = pd.DataFrame(dump, columns=columns)
    
    mos = mos_names_csv()
    df = pd.merge(df, mos, on="imgfile")
    df.to_csv(feat_file, index=None)

# --------------------------------------------------------------------------- #
# ----------- Função de execução direta do script features.py --------------- #
# --------------------------------------------------------------------------- #
if __name__ == "__main__":
    # Gera imagem de exemplo do pré-processamento MSCN
    print("Imagens de exemplo da normalização MSCN...")
    plot_mscn(os.path.join(REFERENCE_FOLDER, "I23.BMP"))
    plot_mscn_rgb(os.path.join(REFERENCE_FOLDER, "I23.BMP"))
    
    # Calcula os atributos RGB_BRISQUE na base inteira e salva em CSV
    print("Calculando atributos...")
    generate_features(brisque_rgb_features, FEATURES_FILE)
    
    
    
    
