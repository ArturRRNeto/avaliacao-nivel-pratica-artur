# file:        eda.py
# author:      Artur Rodrigues Rocha Neto
# email:       artur.rodrigues26@gmail.com
# created:     19/01/2021
# description: Análise exploratória de dados no dataset TID2008.

from nriqa import *

import os
import parse
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from skimage import io
from scipy.stats import spearmanr

def plot_sample_images():
    """
    plot_sample_images()
    Imagens de referência do dataset TID2008. Exibição em tela e arquivo.
    
        Parâmetros
        ----------
            Nenhum.
        Retorno
        -------
            Nada.
    """
    
    images = [image for image in os.listdir(REFERENCE_FOLDER)]
    images.sort()
    
    sns.set()
    fig, axes = plt.subplots(5, 5, sharex=True, figsize=(10, 8))
    fig.suptitle("Images de referência do dataset TID2008")
    
    for index, image in enumerate(images):
        img = io.imread(os.path.join(REFERENCE_FOLDER, image))
        axes[int(index / 5), index % 5].axis("off")
        axes[int(index / 5), index % 5].imshow(img)
    
    plt.tight_layout()
    plt.savefig(os.path.join(FIGURES_FOLDER, "01_images_referencia_TID2008.png"))
    plt.show()

def plot_distortion_images():
    """
    plot_distortion_images()
    Exemplos de imagens distorcidas do dataset TID2008. Exibição em tela e arquivo.
    
        Parâmetros
        ----------
            Nenhum.
        Retorno
        -------
            Nada.
    """
    
    images = [
        "i01_15_1.bmp", "i02_09_4.bmp", "i03_09_4.bmp", "i04_06_4.bmp", "i05_04_4.bmp",
        "i06_06_4.bmp", "i07_04_4.bmp", "i08_15_2.bmp", "i09_09_3.bmp", "I10_02_4.bmp",
        "I11_07_3.bmp", "I12_10_3.bmp", "I13_08_4.bmp", "I14_12_4.bmp", "I15_11_2.bmp",
        "I16_03_3.bmp", "I17_01_4.bmp", "I18_05_3.bmp", "I19_13_3.bmp", "I20_14_4.bmp",
        "I21_16_4.bmp", "I22_17_4.bmp", "I23_12_2.bmp", "I24_14_3.bmp", "i25_08_3.bmp",
    ]
    
    sns.set()
    fig, axes = plt.subplots(5, 5, sharex=True, figsize=(10, 8))
    fig.suptitle("Exemplos de images distorcidas do dataset TID2008")
    
    for index, image in enumerate(images):
        img = io.imread(os.path.join(DISTORTED_FOLDER, image))
        axes[int(index / 5), index % 5].axis("off")
        axes[int(index / 5), index % 5].imshow(img)
    
    plt.tight_layout()
    plt.savefig(os.path.join(FIGURES_FOLDER, "02_images_distorcidas_TID2008.png"))
    plt.show()

def friqa_TID2008_csv(filename="friqa_tid2008.csv"):
    """
    friqa_TID2008_csv(filename="friqa_tid2008.csv")
    Gera um CSV com os atributos cálculados via FR-IQA da base TID2008.
    
        Parâmetros
        ----------
            filename: nome do arquivo CSV que armazenará os atributos.
        Retorno
        -------
            Nada.
    """
    
    # código de metadata de referência
    # importante no futuro
    metadata = {}
    comp = parse.compile("i{}_{}_{}.bmp")
    with open(MOS_NAMES_FILE, "r") as f:
        lines = f.readlines()
        metadata["imgfile"] = [x.strip().split()[1].lower() for x in lines]
        metadata["distortion"] = [int(comp.parse(x.lower())[1]) for x in metadata["imgfile"]]
        metadata["mos"] = [float(x.strip().split()[0]) for x in lines]
    
    features = {}
    for feat in FULLREF_FEATURES:
        with open(os.path.join(METRICS_FOLDER, feat+".txt"), "r") as f:
            features[feat] = [float(x.strip()) for x in f.readlines()]
    
    data = {}
    data.update(metadata)
    data.update(features)
    
    df = pd.DataFrame(data)
    df.set_index("imgfile", inplace=True)
    df.to_csv(os.path.join(DATA_FOLDER, filename))

def friqa_TID2008_vis(filename="friqa_tid2008.csv"):
    """
    friqa_TID2008_vis(filename="friqa_tid2008.csv")
    Análise exploratória visual dos atributos FR da base TID2008.
    
        Parâmetros
        ----------
            filename: nome do arquivo CSV que contém os atributos.
        Retorno
        -------
            Nada.
    """
    
    df = pd.read_csv(os.path.join(DATA_FOLDER, filename))
    
    # MOS por (algumas) classes
    CLASSES = {
        1  : "Ruído gaussiano (intensidade)",
        2  : "Ruído gaussiano (cor)",
        6  : "Ruído por impulsos",
        11 : "Compressão JPEG2000",
        12 : "Erro de transmissão JPEG",
        14 : "Ruído não-centralizado",
        15 : "Distorção em blocos",
        16 : "Mudança de luminosidade",
        17 : "Mudança de contraste",
    }
    
    sub = df.drop(["imgfile", "ssim", "vif", "psnrhvs", "psnrhvsm"], axis=1)
    
    sns.set()
    fig, axes = plt.subplots(3, 3, sharex=True, figsize=(10, 8))
    fig.suptitle("Relação entre uma métrica de referência (SSIM) e o padrão-ouro (MOS)")
    
    for index, series in enumerate([df[df["distortion"] == x] for x in CLASSES.keys()]):
        i = int(index / 3)
        j = index % 3
        
        sns.regplot(x=series["ssim"], y=series["mos"], ax=axes[i, j])
        axes[i, j].set_title(CLASSES[series.iloc[0]["distortion"]])
    
    plt.tight_layout()
    plt.savefig(os.path.join(FIGURES_FOLDER, "03_ssim_mos_TID2008.png"))
    plt.show()
    
    # correlação dos atributos FR
    feats = df.drop(["imgfile", "distortion"], axis=1)
    sns.set_style("darkgrid")
    
    plt.figure(figsize=(10, 8))
    plt.title("Coeficiente de Spearman entre atributos FR e MOS")
    
    sp, _ = spearmanr(feats.to_numpy())
    labels = ["mos"] + FULLREF_FEATURES
    sns.heatmap(sp, annot=True, vmin=0, vmax=1, cmap="mako", xticklabels=labels, yticklabels=labels)
    
    plt.tight_layout()
    plt.savefig(os.path.join(FIGURES_FOLDER, "04_spearman_fr_mos.png"))
    plt.show()
    plt.close()

# --------------------------------------------------------------------------- #
# ------------- Função de execução direta do script eda.py ------------------ #
# --------------------------------------------------------------------------- #
if __name__ == "__main__":
    # Exibe exemplos de imagens da base
    plot_sample_images()
    plot_distortion_images()
    
    # Gera CSV com atributos de referência e algumas visualizações
    friqa_TID2008_csv()
    friqa_TID2008_vis()
    
    
    
    
