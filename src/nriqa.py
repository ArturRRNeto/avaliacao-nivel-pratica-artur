# file:        nriqa.py
# author:      Artur Rodrigues Rocha Neto
# email:       artur.rodrigues26@gmail.com
# created:     24/01/2021
# description: Constantes usadas ao longo de todos os scripts.

DATASET_FOLDER   = "../tid2008/"
METRICS_FOLDER   = "../tid2008/metrics_values/"
MOS_NAMES_FILE   = "../tid2008/mos_with_names.txt"
DISTORTED_FOLDER = "../tid2008/distorted_images/"
REFERENCE_FOLDER = "../tid2008/reference_images/"
BIN_FOLDER       = "../bin/"
DATA_FOLDER      = "../data/"
FIGURES_FOLDER   = "../figures/"

"""
FEATURES_FILE
Caminho completo para os atributos usados.
Constante usada para facilitar a execução dos diversos scripts.
"""
FEATURES_FILE    = "../data/brisque_rgb.csv"

"""
FULLREF_FEATURES
Lista com algumas métricas de referência já pré-calculadas na base TID2008.
"""

FULLREF_FEATURES = ["mssim", "ssim", "vif", "psnrhvs", "psnrhvsm"]

"""
CLASSES
Diciónario que relaciona o índice de uma classe ao seu nome.
Adaptado da base TID2008 tal como no artigo.
"""

CLASSES = {
    1  : "Ruído gaussiano (intensidade)",
    2  : "Ruído gaussiano (cor)",
    3  : "Ruído espacialmente correlacionado",
    4  : "Ruído por máscara",
    5  : "Ruído de alta frequência",
    6  : "Ruído por impulsos",
    7  : "Ruído quantizado",
    8  : "Borramento gaussiano",
    9  : "Filtragem de ruído",
    10 : "Compressão JPEG",
    11 : "Compressão JPEG2000",
    12 : "Erro de transmissão JPEG",
    13 : "Erro de transmissão JPEG2000",
    14 : "Ruído de padrão não-centralizado",
    15 : "Distorção em blocos",
    16 : "Mudança de intensidade",
    17 : "Mudança de contraste",
}

"""
SUBSETS
Agrupamento de classes da TID2008 como sugerido pelos autores.
O grupo "other" foi criado por mim para acumular distorções sem conjunto.
"""

SUBSETS = {
    "noise"       : [1, 3, 5, 6, 7, 8, 9],
    "jpeg"        : [10, 11],
    "exotic"      : [6, 14, 15],
    "actual"      : [1, 3, 6, 7, 8, 9, 10, 11],
    "other"       : [2, 4, 12, 13, 16, 17],
}

