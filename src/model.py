# file:        model.py
# author:      Artur Rodrigues Rocha Neto
# email:       artur.rodrigues26@gmail.com
# created:     20/01/2021
# description: Calibragem do sistema de NR-IQA proposto.

from nriqa import *
from features import *

import os
import operator
import numpy as np
import pandas as pd
import seaborn as sns
from joblib import dump
from joblib import load
from sklearn.svm import SVC
from sklearn.svm import SVR
import matplotlib.pyplot as plt
from scipy.stats import spearmanr
from sklearn.pipeline import Pipeline
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PowerTransformer
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

def optmize_regressor(data_path):
    """
    fit_regressor(data_path)
    Otimiza um regressor para cálculo da métrica de qualidade de imagem.
    
        Parâmetros
        ----------
            data_path: caminho para arquivo CSV com N atributos no formato:
                       [imgfile, distortion, f0, f1, ..., fN-1, mos]
        Retorno
        -------
            Nada.
    """
    
    results = []
    
    for label_index, label_name in CLASSES.items():
        df = pd.read_csv(data_path)
        df = df[df["distortion"].isin([label_index])]
        df.drop(["imgfile", "distortion"], axis=1, inplace=True)
        X = df.drop(["mos"], axis=1)
        y = df["mos"]
        
        steps = [
            ("scaler", StandardScaler(with_std=False)),
            ("power_transformer", PowerTransformer()),
            ("pca", PCA(whiten=True)),
            ("svr", SVR()),
        ]
        
        pipe = Pipeline(steps)
        
        param_grid = {
            "svr__C" : list(np.linspace(1.0, 10.0, 10)),
            "pca__n_components" : list(range(10, 90, 10)),
        }
        
        search = GridSearchCV(pipe, param_grid, n_jobs=-1, cv=5, verbose=2)
        search.fit(X, y)
        
        ans = {
            "label_index" : label_index,
            "label_name" : label_name,
            "best_score" : search.best_score_,
        }
        
        ans.update(search.best_params_)
        results.append(ans)
    
    results = pd.DataFrame(results)
    results.to_csv("../data/regressor_optmization.csv", index=None)
    print(results)

def create_regressor(X_train, y_train, X_test, y_test, **kwargs):
    """
    create_regressor(X_train, y_train, X_test, y_test, **kwargs)
    Ajusta um regressor com base no pipeline escolhido.
    
        Parâmetros
        ----------
            X_train: atributos de treinamento.
            y_train: classes de treinamento.
            X_test: atributos de teste.
            y_test: classes de teste.
            **kwargs: dicionário com os hiper-parâmetros ótimos do regressor.
        Retorno
        -------
            reg: o regressor ajustado.
            spear: o coeficiente de Spearman resultante da predição do teste.
    """
    
    steps = [
        ("scaler", StandardScaler(with_std=False)),
        ("power_transformer", PowerTransformer()),
        ("pca", PCA(n_components=kwargs["n_components"], whiten=True)),
        ("svr", SVR(C=kwargs["C"])),
    ]
    
    reg = Pipeline(steps)
    reg.fit(X_train, y_train)
    y_pred = reg.predict(X_test)
    spear, _ = spearmanr(y_test, y_pred)
    
    return reg, spear

def create_classifier(X_train, y_train, X_test, y_test):
    """
    create_classifier(X_train, y_train, X_test, y_test)
    Ajusta um clasificador com base no pipeline escolhido.
    
        Parâmetros
        ----------
            X_train: atributos de treinamento.
            y_train: classes de treinamento.
            X_test: atributos de teste.
            y_test: classes de teste.
        Retorno
        -------
            clf: o classificador ajustado.
            score: a acurácia de classificação resultante da predição do teste.
    """
    
    steps = [
        ("scaler", StandardScaler(with_std=False)),
        ("power_transformer", PowerTransformer()),
        ("linear_discriminat", LinearDiscriminantAnalysis()),
        ("svm", SVC(C=1.0, kernel="rbf", probability=True)),
    ]
    
    clf = Pipeline(steps)
    clf.fit(X_train, y_train)
    score = clf.score(X_test, y_test)
    
    return clf, score

def estimator(img_path):
    """
    estimator(img_path)
    Carrega estimador de distorção e nota e avalia uma imagem.
    
        Parâmetros
        ----------
            img_path: caminho para a imagem a ser avaliada.
        Retorno
        -------
            label: etiqueta da classe (1 a 17).
            class_name: nome subjetivo da classe.
            score: nota de qualidade.
    """
    
    # carrega persistência do classificador
    clf = load(os.path.join(BIN_FOLDER, "clf.joblib"))
    
    # extrái atributos da imagem
    X = np.array(brisque_rgb_features(io.imread(img_path))).reshape(1, -1)
    
    # calcula classe e pertencimentos da imagem
    label = clf.predict(X)[0]
    class_name = CLASSES[label]
    
    # carrega regressor específico e calcula nota
    reg = load(os.path.join(BIN_FOLDER, "reg_{}.joblib".format(label)))
    score = round(reg.predict(X)[0], 4)
    
    return label, class_name, score

def evaluate_estimator():
    """
    evaluate_estimator()
    Carrega estimador para todas as imagens da TID2008 e avalia performance geral.
    
        Parâmetros
        ----------
            Nenhum.
        Retorno
        -------
            Nada.
    """
    
    images = []
    y_pred = []
    y_true = []
    score_pred = []
    score_true = []
    comp = parse.compile("i{}_{}_{}.bmp")
    
    df = pd.read_csv("../data/friqa_tid2008.csv")
    df.set_index("imgfile", inplace=True)
    
    for img_file in os.listdir(DISTORTED_FOLDER):
        imgfile = img_file.lower()
        
        label_real = int(comp.parse(imgfile)[1])
        mos = df.loc[imgfile]["mos"]
        y_true.append(label_real)
        score_true.append(mos)
        
        img_path = os.path.join(DISTORTED_FOLDER, img_file)
        label, class_name, score = estimator(img_path)
        
        y_pred.append(label)
        score_pred.append(score)
        
        images.append(imgfile)
        
        print("{}: {} x {} - {} x {}".format(imgfile, label_real, label, mos, score))
    
    ans = {
        "imgfile"    : images,
        "y_pred"     : y_pred,
        "y_true"     : y_true,
        "score_pred" : score_pred,
        "score_true" : score_true
    }
    
    df = pd.DataFrame(ans)
    df.to_csv("../data/estimator_evaluation.csv", index=None)

def statistics_estimator(stats_file):
    """
    statistics_estimator(stats_file)
    Avalia, em termos de matriz de confusão, acurácia, SPEAR e RMSE a qualidade do estimador.
    
        Parâmetros
        ----------
            stats_file: dataframe contendo os dados para avaliação.
        Retorno
        -------
            Nada.
    """
    
    from sklearn.metrics import accuracy_score
    from sklearn.metrics import mean_squared_error
    from sklearn.metrics import confusion_matrix
    
    df = pd.read_csv(stats_file)
    
    acc = accuracy_score(df["y_true"], df["y_pred"])
    rmse = mean_squared_error(df["score_true"], df["score_pred"])
    spear, _ = spearmanr(df["score_true"], df["score_pred"])
    conf_mat = confusion_matrix(df["y_true"], df["y_pred"])
    
    print("Acurácia geral: {}".format(round(acc, 4)))
    print("RMSE geral: {}".format(round(rmse, 4)))
    print("SPEAR geral: {}".format(round(spear, 4)))
    
    sns.set()
    plt.figure(figsize=(10, 8))
    plt.title("Matriz de confusão do estimador final")
    
    ticks = list(range(1, 18))
    sns.heatmap(conf_mat, annot=True, fmt="d", xticklabels=ticks, yticklabels=ticks)
    
    plt.tight_layout()
    plt.savefig(os.path.join(FIGURES_FOLDER, "matriz_confusao_estimador.png"))
    plt.show()
    
    for k, v in CLASSES.items():
        sub = df[df["y_true"] == k]
        
        rmse = mean_squared_error(sub["score_true"], sub["score_pred"])
        spear, _ = spearmanr(sub["score_true"], sub["score_pred"])
        
        print(k, "\t", round(spear, 4), "\t", round(rmse, 4))

# --------------------------------------------------------------------------- #
# ------------ Função de execução direta do script model.py ----------------- #
# --------------------------------------------------------------------------- #
if __name__ == "__main__":
    
    # Encontra os melhores parâmetros do regressor
    # !!! executar apenas quando necessário pois demora pra caramba !!!
    optmize_regressor(FEATURES_FILE)
    
    df = pd.read_csv(FEATURES_FILE)
    
    # Cria classificador para todas as classes da TID2008
    # Persitência dos dados no diretório ../bin/
    print("Ajustando classificador...")
    df_clf = df.copy()
    X = np.array(df_clf.drop(["imgfile", "distortion", "mos"], axis=1))
    y = np.ravel(df_clf["distortion"])
    
    results = {
        "clf"   : [],
        "score" : [],
    }
    
    skf = StratifiedKFold(n_splits=5)
    for train_index, test_index in skf.split(X, y):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        
        clf, score = create_classifier(X_train, y_train, X_test, y_test)
        
        results["clf"].append(clf)
        results["score"].append(score)
        
    mi = results["score"].index(max(results["score"]))
    best_clf = results["clf"][mi]
    best_score = results["score"][mi]
    dump(best_clf, os.path.join(BIN_FOLDER, "clf.joblib"))
    
    print("Melhor classificador obtido: score = {}".format(round(best_score*100, 4)))
    print("Classificador ajustado!")
    
    # Cria regressores, um para cada classe da TID2008
    # Carrega valores de C e n_components da tabela previamente criada na etapa de otimização
    # Persitência dos dados no diretório ../bin/
    print("Ajustando regressores...")

    reg_opti = pd.read_csv(os.path.join(DATA_FOLDER, "regressor_optmization.csv"))
    reg_opti.set_index("label_index", inplace=True)
    reg_opti.drop(["label_name", "best_score"], axis=1, inplace=True)
    reg_opti.rename(columns={"pca__n_components" : "n_components", "svr__C" : "C"}, inplace=True)
    
    for index, row in reg_opti.iterrows():
        reg_data = df[df["distortion"] == index]
        X = np.array(reg_data.drop(["imgfile", "distortion", "mos"], axis=1))
        y = np.ravel(reg_data["mos"])
        
        results = {
            "reg"   : [],
            "spear" : [],
        }
        for i in range(10):
            X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
            reg, spear = create_regressor(X_train,
                                          y_train,
                                          X_test,
                                          y_test,
                                          C=row["C"],
                                          n_components=int(row["n_components"]))
            
            results["reg"].append(reg)
            results["spear"].append(spear)
        
        mi = results["spear"].index(max(results["spear"]))
        best_reg = results["reg"][mi]
        best_spear = results["spear"][mi]
        
        dump(best_reg, os.path.join(BIN_FOLDER, "reg_{}.joblib".format(index)))
        print("Melhor regressor para classe {}: spearman = {}".format(index, round(best_spear, 4)))
    
    print("Regressores ajustados!")
    
    
    
    
