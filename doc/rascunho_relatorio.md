# Relatório

Elementos:

- Introdução e contextualização
    - FR/RR/NR-IQA
- Análise exploratória
    - classes
    - imagens de referência [OK]
    - imagens distorcidas [OK]
    - spearman: métricas FR e MOS [OK]
    - visualização do MOS por classe vs ssim [OK]
- Descrição da estratégia
    - seleção de dados
    - extrator de atributos
    - sistema
        - classificador
        - regressor
- Aplicação
    - CLI
    - GUI

### Calibragem de hiperparâmetros do classificador

ROUND 1
param_grid = {
    "mlp__alpha" : [0.0001, 0.05],
    "mlp__hidden_layer_sizes" : [(100,), (200,), (500,)],
    "mlp__max_iter" : [10000],
    "mlp__activation" : ["relu", "logistic"],
    "mlp__solver" : ["adam", "sgd"],
    "mlp__learning_rate" : ["constant", "adaptive"],
}
Best parameter (CV score=0.870):
{
    'mlp__activation': 'relu',
    'mlp__alpha': 0.05,
    'mlp__hidden_layer_sizes': (500,),
    'mlp__learning_rate': 'constant',
    'mlp__max_iter': 10000,
    'mlp__solver': 'adam'
}

ROUND 2
param_grid = {
    "mlp__alpha" : [0.0001, 0.005, 0.001, 0.05, 0.1],
    "mlp__hidden_layer_sizes" : [(500,), (600,), (800,), (1000,)],
    "mlp__max_iter" : [20000],
}
Best parameter (CV score=0.869):
{
    'mlp__alpha': 0.0001,
    'mlp__hidden_layer_sizes': (1000,),
    'mlp__max_iter': 20000
}

