# Guia de Estudo

Por se tratar de um problema nunca abordado por mim durante a minha formação, seguirei este guia de estudo simples que pretende revelar (no curto período de tempo disponível) o máximo sobre o problema de NR-IQA e como parte da literatura vem a tratando.

1. Assistir ao vídeo "DT201A. Non-Reference Picture Quality Assessment"
	- qual o definição/conceituação de NR-IQA?
	- quais os elementos de um sistema NR-IQA?
	- qual a abordagem do modelo proposto?

2. Ler o artigo "TID2008 - A Database for Evaluation of Full-Reference Visual Quality Assessment Metrics"
	- como é o dataset?
	- quais classes ele contém?
	- como é medida a qualidade (MOS) da verdade terrestre?
	- qual a relação entre distorção, situação prática e HVS?
	- como visualizar as informações contidas (MOS, classes, correlações)?
	    - quantas amostras por cada classe de distorção estão disponíveis?
	    - qual a relação imagem x MOS de cada amostra?
	    - qual a média/variância entre os MOS inter e intraclasses/imagens?

3. Ler o artigo "No-reference image quality assessment algorithms: A survey"
	- qual foi o perfil traçado pela literatura de 2008 a 2015?
	- quais os métodos mais usados até aquele momento?
	- como se enquadra o dataset TID2008 dentre as avaliações?
	- os métodos se preocupam mais em quantificar a qualidade, classificar a distorção ou ambos?
	- quais distorções são as mais atacadas?
	- como comparar o método em diferentes bases de dados?

4. Ler o artigo "A Two-Step Framework for Constructing Blind Image Quality Indices"
	- em questão à classificação de distorções, como a literatura inicialmente propôs o modelo preditivo?
	    - quais são as suas limitações? (spoiler: grande interseção interclasses)
	- quais atributos foram extraídos?
	- qual classificador foi escolhido?
	- quais as perguntas de pesquisa ficaram em aberto?

