# Desafio De Visão Computacional

Projeto disponibilizado em:

    http://pdi.mobitbrasil.com.br:8601/scm/anp/projetista-sis-embarc-deeplearning

Para realizar o teste, faça o clone do repositório em sua máquina local.

    http://pdi.mobitbrasil.com.br:8601/scm/anp/projetista-sis-embarc-deeplearning.git

Utilize o e-mail jobs@mobitbrasil.com.br para dúvidas e esclarecimentos.

## Problema:
Sistemas de visão computacional são completamente dependentes do sistema de captura. A má qualidade do ajuste de câmera pode prejudicar todo o desempenho do sistema. Precisamos de um software que identifique quando a câmera está mal ajustada.
## Objetivo:
Sua tarefa é desenvolver um sistema de visão computacional que quantifique a qualidade da imagem e indique que tipo de problema essa imagem possui. A única entrada que o algoritmo obtém é a imagem cuja qualidade você deseja medir. Esse problema é conhecido como ***No-Reference Image Quality Assessment***.

O problema pode ser resolvido usando python ou c++ combinados com quaisquer bibliotecas e/ou frameworks.
Se você se basear em tutoriais e/ou códigos de exemplo ou copiar trechos de código de algum lugar, por favor indique esses trechos explicitamente. Inclua em sua solução também o código fonte para treinamentoe e validações que você utilizou para obter o modelo final.

## Dataset:
Utilize o banco de dados TID2008, que foi disponibilizado para fins de pesquisa. Encontrado em:

    https://mobit365-my.sharepoint.com/:u:/g/personal/edsoncavalcanti_mobitbrasil_com_br/ER_P4aQljipChw3tReM38iABbeFTY6VU-Q9ji4KOBhWMAw?e=DAqUqx

## Entregas:
- **Análise exploratória dos dados (EDA)**: faça uma avaliação preliminar dos dados fornecidos;
- **Aplicação**: desenvolva um software que receba uma imagem, calcule a qualidade da imagem e classifique que tipo de problema essa imagem possui. Pode ser uma aplicação em linha de comando, que imprima o resultado no terminal.
- **Relatório**: formalize em um relatório a apresentação da estratégia, metodologia e arquitetura utilizadas, assim como os resultados obtidos. Pode ser em slides (.ppt e afins), texto (PDF) ou mesmo um notebook do Jupyter.
- **Commits**: Você deverá registrar todos os seus commits neste repositório. Queremos ver o passo-a-passo do desenvolvimento da aplicação.
- **Documentação**: Parte da nossa avalição envolve executar seu código no nosso ambiente, então você precisa nos fornecer as instruções necessárias para fazê-lo. Você pode utilizar ferarmentas e scritps de deploy, como o docker, que facilitem a execução da aplicação, mas ainda assim deve fornecer o código fonte da solução.

    * **BONUS**: Desenvolva uma API REST ou uma interface grafica (GUI) para realizer a inferência de novas imagens.


Quando finalizado e pronto para envio, gere o(s) arquivo(s) de patch com os códigos desenvolvidos.
Envie os arquivos de patch gerados por e-mail ao responsável pela aplicação do teste (jobs@mobitbrasil.com.br) com o git configurado para envio de e-mail.

Caso prefira, pode clonar este repositório e subir as soluções em sua própria conta do Bitbucket. Nesse caso, enviar o endereço do repositório com as soluções para o email (jobs@mobitbrasil.com.br).

Em caso de dúvidas, pergunte. Boa sorte!
