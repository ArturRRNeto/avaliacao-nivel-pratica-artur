# Desafio de Visão Computacional

Avaliação de nível prática em *No-Reference Image Quality Assessment* (NR-IQA): desenvolvimento de um sistema de visão computacional que quantifique a qualidade da imagem e indique que tipo de problema a imagem possui. O enunciado original completo do desafio pode ser encontrado no arquivo `doc/enunciado_desafio.md`.

## Resumo da estratégia

O relatório completo da estratégia pode ser encontrado no arquivo `doc/artur_relatorio_NRIQA.pdf`.

Este repositório apresenta uma solução de sistema de visão computacional para avalição qualitativa sem referência (NR-IQA) baseado em estatísticas de cena (BRISQUE_RGB) e um estimador hierárquico em duas etapas para atribuição de qualidade e inferência de distorção (Máquina Vetor de Suporte para regressão e classificação), com objetivo de auxiliar no ajuste de dispositivos de aquisição. Treinado na base TID2008, o sistema proposto é capaz de classificar com acurácia geral de 88.06% para todas as 17 classes de distorção do conjunto de dados, além de estimar uma nota qualitativa com alta correlação com o padrão-ouro (Spearman geral: 0.9097, RMSE geral: 0.2796).

## Conteúdo do repositório

- `bin`: estimadores treinados
- `data`: dataframes com atributos extraídos, hiperparâmetros otimizados e outras estatísticas
- `doc`:  apresentação da proposta, relatório detalhado, notas de estudo, referências
- `figures`: imagens de análise exploratória, resultados, diagramas
- `src`: códigos-fonte das experimentações, aplicação CLI

## Obtendo os arquivos

Primeiro, clone o repositório no seu computador executando:

```
git clone https://bitbucket.org/ArturRRNeto/avaliacao-nivel-pratica-artur.git
```

A seguir, baixe a base de dados TID2008 usando o link:

```
https://mobit365-my.sharepoint.com/:u:/g/personal/edsoncavalcanti_mobitbrasil_com_br/ER_P4aQljipChw3tReM38iABbeFTY6VU-Q9ji4KOBhWMAw?e=DAqUqx
```

Descompacte-a na raíz deste repositório. A estrutura de arquivos final (com destaque à estrutura do dataset) deve ser:

```
+-- avaliacao-nivel-pratica-artur/
|   +-- bin/
|   +-- data/
|   +-- doc/
|   +-- figures/
|   +-- src/
|   +-- tid2008/
|   |   +-- distored_images/
|   |   +-- metrics_values/
|   |   +-- papers/
|   |   +-- reference_images/
|   |   +-- mos.txt
|   |   +-- mos_std.txt
|   |   +-- mos_with_names.txt
|   |   +-- readme
|   +-- README.md
```

## Dependências

Uma vez com a estrutura de arquivos está pronta, é hora de instalar as dependências da solução. Os scripts foram escritos Python 3.8+; versões menores vão funcionar, mas você precisará recalcular o estimador para adequação de versões (ver **Gerando o sistema**).

Todas as dependências podem ser instaladas usando o gerenciador de pacotes `pip3`. Primeiro, certifique-se que ele está atualizado executando:

```
pip3 install --upgrade pip
```

A seguir, execute:

```python
pip3 install --user parse numpy scipy pandas matplotlib seaborn scikit-learn scikit-image opencv-python
```

Para melhor aproveitar o pacote `scikit-image`, recomenda-se atualizar a dependência `pillow` para a versão mais atual:

```python
pip3 install --user --upgrade pillow
```

Se você não tiver acesso ao gerenciador `pip3`, tente trocar as chamadas apenas por `pip` (não existe padronização de nomes ao longo de distribuições Python). Se você não possui o `pip3` instalado (ou mesmo o Python), instale-os usando o gerenciador de pacotes do seus sistema operacional. No Debian e derivados você pode fazê-lo executando:

```
sudo apt install python3-dev python3-pip
```

## Os scripts

Os scripts em `src/` correspondem a uma pequena biblioteca de funções, cada um com funcionalidade diferentes. Eles podem ser usados através de `import` para uso de suas funções em outros scripts, mas também podem ser executados diretamente com `python3` para execução de algumas experimentações. A seguir uma descrição de cada um:

- `eda.py`: funções de análise exploratória dos dados; se executado, gera imagens de referência e uma tabela contendo alguns dos atributos FR da base.
- `features.py`: funções de extração de atributos; ao executar, calcula todos os atributos BRISQUE_RGB da base TID2008 e salva em arquivo, além de plotar imagens da normalização MSCN.
- `model.py`: funções de criação e otimização dos estimadores (classificador e regressor); se executado diretamente, ajusta os melhores estimadores para o conjunto de atributos extraído e salva persitência dos modelos no diretório `bin/`.
- `nriqa.py`: arquivo de constantes usadas ao longo dos outros scritps; não faz nada em execução direta.
- `cli.py`: aplicação de interface de linha de comando que recebe o caminho para uma imagem e retorna: classe de distorção a qual ela pertence e nota objetiva de qualidade.

## Gerando o sistema

Versões do estimador já foram pré-calculadas e estão salvas na pasta `bin/`. Entretanto, por questão de diferença de versões da biblioteca `sciki-learn` usada para compilá-los e a que você usará para execução, o formato de persistência pode mudar.

A sequência de execução que gera todos os atributos e estimadores no caso de novos experimentos é:

```python
python3 features.py
python3 model.py
```

Para gerar os dados da etapa de exploração da base TID2008, basta executar:

```python
python3 ead.py
```

## Aplicação CLI

A aplicação de linha de comando pode ser encontrada em `src/cli.py`. Seu uso é bem simples:

```python
python3 cli.py <arquivo_imagem>
```

A resposta retornada é a classe de distorção a qual pertence a imagem (nome e etiqueta) e a nota de qualidade aferida, como no exemplo abaixo:

![cli](https://i.imgur.com/uYfLnfj.png)
## Contato

- Email: [artur.rodrigues26@gmail.com](mailto:artur.rodrigues26@gmail.com)

